<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $guarded = [];

    public function __construct(string $title, string $body, string $image)
    {
        parent::__construct([
            'title' => $title,
            'body' => $body,
            'image' => $image,
        ]);
    }

}
