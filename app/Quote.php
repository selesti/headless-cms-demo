<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{

    protected $guarded = [];

    public function __construct(string $title, string $body)
    {
        parent::__construct([
            'title' => $title,
            'body' => $body
        ]);
    }

}
