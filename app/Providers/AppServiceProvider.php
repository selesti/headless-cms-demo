<?php

namespace App\Providers;

use App\ContentProviders\ButterCmsContentProvider;
use App\ContentProviders\ContentContract;
use App\ContentProviders\ContentfulContentProvider;
use App\ContentProviders\PrismicContentProvider;
use Illuminate\Support\ServiceProvider;
use App\ContentProviders\HardcodedContentProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ContentContract::class, HardcodedContentProvider::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

}
