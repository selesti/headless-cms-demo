<?php

namespace App\ContentProviders;

use App\Page;
use Exception;
use Illuminate\Support\Collection;

/**
 * Class AbstractContent
 * @package App\ContentProviders
 */
abstract class AbstractContentProvider
{

    /**
     * @var Page
     */
    protected $page;

    /**
     * @var string
     */
    public $pageId;

    /**
     * @param string $pageId
     * @return Page
     * @throws Exception
     */
    public function get(string $pageId) : Page
    {
        $this->pageId = $pageId;
        $this->page = $this->addPage();

        $this->addProducts();

        $this->addQuotes();

        if (!is_a($this->page->products, Collection::class)) {
            throw new Exception('A collection of products are required.');
        }

        if (!is_a($this->page->quotes, Collection::class)) {
            throw new Exception('A collection of quotes are required.');
        }

        return $this->page;
    }

    /**
     * @return Page
     */
    abstract public function addPage() : Page;

    /**
     * @return void
     */
    abstract public function addProducts() : void;

    /**
     * @return void
     */
    abstract public function addQuotes() : void;

}
