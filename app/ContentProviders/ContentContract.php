<?php

namespace App\ContentProviders;

use App\Page;

/**
 * Interface ContentContract
 * @package App\ContentProviders
 */
interface ContentContract
{

    /**
     * @return Page
     */
    public function addPage() : Page;

    /**
     * @return void
     */
    public function addProducts() : void;

    /**
     * @return void
     */
    public function addQuotes() : void;

}
