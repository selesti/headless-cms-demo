<?php

namespace App\ContentProviders;

use App\Page;
use App\Quote;
use App\Product;

/**
 * Class HardcodedService
 * @package App\ContentProviders
 */
class HardcodedContentProvider extends AbstractContentProvider implements ContentContract
{

    /**
     * @return Page
     */
    public function addPage(): Page
    {
        return new Page('My headless shop!', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ab at culpa cumque cupiditate deleniti enim in, ipsam iure minima neque placeat quas quidem quis quos recusandae sed sunt totam?');
    }

    /**
     * @return void
     */
    public function addProducts() : void
    {
        $this->page->setProducts(
            collect([
                new Product('Fancy Product 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit', 'https://images.unsplash.com/photo-1507622488581-687d4ac07a3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'),
                new Product('Shiny Product 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit', 'https://images.unsplash.com/photo-1507622488581-687d4ac07a3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'),
                new Product('Cool Product 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit', 'https://images.unsplash.com/photo-1507622488581-687d4ac07a3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60')
            ])
        );
    }

    /**
     * @return void
     */
    public function addQuotes() : void
    {
        $this->page->setQuotes(
            collect([
                new Quote('Happy customer', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'),
                new Quote('Annoyed customer', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'),
                new Quote('Returning customer', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.')
            ])
        );
    }

}
