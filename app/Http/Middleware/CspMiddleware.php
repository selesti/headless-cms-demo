<?php

namespace App\Http\Middleware;

use Closure;
use ParagonIE\CSPBuilder\CSPBuilder;

/**
 * Class CspMiddleware
 * @package App\Http\Middleware
 */
class CspMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        $csp = CSPBuilder::fromFile(
            resource_path('csp.json')
        );

        if (app()->environment('local')) {
            array_map(function ($url) use ($csp, $request) {
                $csp->addSource('font', 'https://' . $url);
                $csp->addSource('img', 'https://' . $url);
                $csp->addSource('script', 'https://' . $url);
                $csp->addSource('style', 'https://' . $url);
                $csp->addSource('self', 'https://' . $url);
                $csp->addSource('self', 'wss://' . $url);
                $csp->addSource('connect', 'https://' . $url);
                $csp->addSource('connect', 'wss://' . $url);
                $csp->addSource('connect', 'ws://' . $url);
            }, [
                'localhost:3100', // HMR
                'localhost:8080', // HMR - Alt
                $request->getHttpHost() . ':3100', // BrowserSync
                '127.0.0.1:35729', // LiveReload
            ]);
        }

        // WebSocks
        $csp->addSource('connect', 'wss://' . $request->getHttpHost() . ':6001');
        $csp->addSource('connect', 'https://' . $request->getHttpHost() . ':6001');
        $csp->addSource('connect', 'wss://' . $request->getHttpHost() . ':6002');
        $csp->addSource('connect', 'https://' . $request->getHttpHost() . ':6002');

        if (!headers_sent()) {
            // These should also be set in the .htaccess file for static assets
            header('X-XSS-Protection: 1; mode=block');
            header('X-Frame-Options: SAMEORIGIN');
            header('X-Content-Type-Options: nosniff');
            header('Strict-Transport-Security: max-age=63072000; includeSubDomains; preload');
            header('X-Powered-By: www.selesti.com');
            header('Referrer-Policy: same-origin');

            $csp->sendCSPHeader();
        }

        return $next($request);
    }

}
