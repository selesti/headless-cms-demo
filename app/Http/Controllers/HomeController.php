<?php

namespace App\Http\Controllers;

use App\ContentProviders\ContentContract;

class HomeController extends Controller
{

    /**
     * @var ContentContract
     */
    private $service;

    public function __construct(ContentContract $service)
    {
        $this->service = $service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $page = $this->service->get('homepage');

        return view('home', compact('page'));
    }

}
