<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property Collection products
 */
class Page extends Model
{

    protected $guarded = [];

    /**
     * @var Collection
     */
    public $products;

    /**
     * @var Collection
     */
    public $quotes;

    public function __construct(string $title, string $body)
    {
        parent::__construct([
            'title' => $title,
            'body' => $body
        ]);
    }

    public function setProducts(Collection $products)
    {
        $products->each(function (Product $product) {
            // all okay.
        });

        $this->products = $products;
    }

    public function setQuotes(Collection $quotes)
    {
        $quotes->each(function (Quote $quote) {
            // all okay.
        });

        $this->quotes = $quotes;
    }

}
