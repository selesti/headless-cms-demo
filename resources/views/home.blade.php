@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $page->title }}</div>

                <div class="card-body">

                    {!! $page->body !!}

                </div>
            </div>
        </div>

        <div class="col-md-8 mt-4">

            <div class="card">

                <div class="card-header">What our customers say</div>

                <div class="card-body">
                    <div class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            @foreach ($page->quotes as $quote)
                                <div class="carousel-item {{ $loop->first ? 'active' : '' }}">

                                    <blockquote class="blockquote">
                                        {!! $quote->body !!}
                                        <footer class="blockquote-footer"><cite>{{ $quote->title }}</cite></footer>
                                    </blockquote>

                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="col-md-8 mt-4">

            <div class="row">
                @foreach ($page->products as $product)
                <div class="col-sm-6 mb-4">
                    <div class="card">
                        <div class="card-header">{{ $product->title }}</div>

                        <div class="card-body">

                            <img style="width: 100%;" class="mb-3" src="{{ $product->image }}" alt="Image of {{ $product->title }}.">

                            {!! $product->body !!}

                        </div>
                    </div>
                </div>
                @endforeach
            </div>

        </div>

    </div>
</div>
@endsection
