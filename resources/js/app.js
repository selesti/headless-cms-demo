import 'core-js';
import Vue from 'vue';

require('./bootstrap');

require.context('./', true, /\.vue$/i).keys()
.map(key => Vue.component(key.split('/').pop()
.split('.')[0], files(key)));

if (document.getElementById('app')) {
    new Vue({
        el: '#app',
    });
}
